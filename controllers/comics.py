#Exam No: Y0077439
@auth.requires_login()
def create():
    #COMICS IS BASED ON A CUSTOM FORM except for cover
    form = SQLFORM(db.comics, fields=['title','cover'])
    if request.post_vars:
        form.vars.description = request.post_vars.description
        form.vars.issue_no = request.post_vars.issue_number
        form.vars.owner = auth.user_id
        #Publisher
        publisher = db(db.publisher.name == request.post_vars.publisher).select().first()
        form.vars.publisher = publisher.id if publisher else db.publisher.insert(name=request.post_vars.publisher)

    if form.process().accepted:
        onAccepted(form,True)

    response.boxes = db(db.box.owner == auth.user_id).select(orderby=db.box.name)
    response.persons = db().select(db.person.ALL, orderby=db.person.name)
    response.publishers = db().select(db.publisher.ALL, orderby=db.publisher.name)
    return dict(form=form)


@auth.requires_login()
def delete():
    comics = db(db.comics.id == request.args[0]).select().first()
    db(db.comics.id == request.args[0]).delete()
    first = db(db.box.owner == auth.user_id).select().first()
    redirect(URL('boxes', 'view', args=[first.id], vars=dict(message='removed',m_comics=comics.title)))

def onAcceptedBoxHandler(comics_id):
    #deleting all boxes in the database is simpler than taking NOT(NEW-OLD) set relation
    boxes = db(db.comics_boxes.comics == comics_id).delete()
    if type(request.vars['box[]']) is list:
        first = True
        box2 = 0
        for box in request.vars['box[]']:
            if not first:
                box2 = box
                db.comics_boxes.insert(comics=comics_id,box=box)
            else:
                first = False
        return box2
    else:
        #In the event someone doesn't select any boxes
        unfiled = db((db.box.name == 'Unfiled') & (db.box.owner == auth.user_id)).select().first()
        db.comics_boxes.insert(box=unfiled.id,comics=comics_id)
        return unfiled.id


def onAcceptedWriterHandler(form):
    added = []
    db(db.comics_writers.comics == form.vars.id).delete()
    if type(request.vars['writer[]']) is list :
        for writer in request.vars['writer[]']:
            if writer != '':
                if not writer in added:
                    added.append(writer)
                    w = db(db.person.name == writer).select().first()
                    #If the writer doesn't exist, we create a new one and get the id
                    #of the newly created record
                    if w == None:
                        writer_id = db.person.insert(name=writer)
                    else:
                        writer_id = w.id
                    db.comics_writers.insert(comics=form.vars.id, writer=writer_id)

def onAcceptedArtistHandler(form):
    added = []
    db(db.comics_artists.comics == form.vars.id).delete()
    if type(request.vars['artist[]']) is list :
        for artist in request.vars['artist[]']:
            if artist != '':
                if not artist in added:
                    added.append(artist)
                    a = db(db.person.name == artist).select().first()
                    #If the artist doesn't exist, we create a new one and get the id
                    #of the newly created record
                    if a == None:
                        artist_id = db.person.insert(name=artist)
                    else:
                        artist_id = a.id
                    db.comics_artists.insert(comics=form.vars.id, artist=artist_id)

def onAccepted(form,add):
    box = onAcceptedBoxHandler(form.vars.id)
    onAcceptedWriterHandler(form)
    onAcceptedArtistHandler(form)
    db.commit()
    redirect(URL('comics', 'view', args=[form.vars.id], vars=dict(box=box,message=('newcomics'if add else 'editcomics'))))


@auth.requires_login()
def edit():
    #GETTING THE COMICS
    response.comics = db(db.comics.id == request.args[0]).select().first()
    #CREATING A FORM BASED ON THAT COMICS
    #I only used title and cover, the other field are custom e.g. number
    form = SQLFORM(db.comics, response.comics, fields=['title','cover'])
    if request.post_vars:
        form.vars.description = request.post_vars.description
        form.vars.issue_no = request.post_vars.issue_number
        #Publisher
        publisher = db(db.publisher.name == request.post_vars.publisher).select().first()
        form.vars.publisher = publisher.id if publisher else db.publisher.insert(name=request.post_vars.publisher)

    if form.process(keepvalues=True).accepted:
        #Add the many-to-many relations
        onAccepted(form,False)

    #FETCH TO FILL
    response.c_writers = db(db.comics_writers.comics == response.comics.id).select(join=db.person.on(db.person.id==db.comics_writers.writer))
    response.c_artists = db(db.comics_artists.comics == response.comics.id).select(join=db.person.on(db.person.id==db.comics_artists.artist))
    boxes = db(db.comics_boxes.comics == response.comics.id).select()
    response.c_boxes = []
    for box in boxes:
        response.c_boxes.append(box.box)
    response.c_publisher = db(db.publisher.id == response.comics.publisher).select().first()
    #COMICS IS BASED ON A CUSTOM FORM except for cover
    response.boxes = db(db.box.owner == auth.user_id).select(orderby=db.box.name)
    response.persons = db().select(db.person.ALL, orderby=db.person.name)
    response.publishers = db().select(db.publisher.ALL, orderby=db.publisher.name)
    return dict(form=form)

@auth.requires_login()
def to_box():
    if db((db.comics_boxes.box == request.vars.box) & (db.comics_boxes.comics == request.vars.comics)).count():
        redirect(request.vars.url+('?' if '?' not in request.vars.url else '&')+"message=not_copied")
    comics = db(db.comics.id == request.vars.comics).select().first()
    #we pick the first box that the comics belongs to
    to_b = db(db.box.id == request.vars.box).select().first()
    box = db(db.comics_boxes.comics == comics.id).select(join=db.box.on(db.box.id==db.comics_boxes.box)).first()
    #if the box belongs to a user
    if box.box.owner != auth.user_id:
        new_comic = db.comics.insert(**db.comics._filter_fields(comics))
        db(db.comics.id == new_comic).update(owner = auth.user_id)
        db.comics_boxes.insert(box=request.vars.box,comics=new_comic.id)
        for writer in db(db.comics_writers.comics == comics.id).select():
            db.comics_writers.insert(writer=writer.writer,comics=new_comic)
        for artist in db(db.comics_artists.comics == comics.id).select():
            db.comics_artists.insert(artist=artist.artist,comics=new_comic)
    else:
        new_comic = db.comics_boxes.insert(box=request.vars.box,comics=comics.id)

    if request.vars.url:
        redirect(request.vars.url+('?' if '?' not in request.vars.url else '&')+"message=copied_comics&m_box=" + str(to_b.name) + "&m_comics=" + str(comics.title))
    else:
        redirect(URL('default', 'index'))

@auth.requires_login()
def from_box():
    to_b = db(db.box.id == request.vars.box).select().first()
    comics = db(db.comics.id == request.vars.comics).select().first()
    db((db.comics_boxes.box == request.vars.box) & (db.comics_boxes.comics == request.vars.comics)).delete()
    if db(db.comics_boxes.comics == request.vars.comics).count() == 0:
        unfiled = db((db.box.name == 'Unfiled') & (db.box.owner == auth.user_id)).select().first()
        db.comics_boxes.insert(box=unfiled.id,comics=request.vars.comics)
        redirect(request.vars.url+('?' if '?' not in request.vars.url else '&')+"message=in_unfiled&m_box=" + str(to_b.name) + "&m_comics=" + str(comics.title))

    redirect(request.vars.url+('?' if '?' not in request.vars.url else '&')+"message=removed&m_box=" + str(to_b.name) + "&m_comics=" + str(comics.title))

def view():
    response.comics_id = request.args[0]
    response.comics = db(db.comics.id == response.comics_id).select().first()
    response.box = db(db.box.id == request.vars.box).select().first()

    response.publisher = db(db.publisher.id == response.comics.publisher).select().first()
    response.writers = db((db.person.id == db.comics_writers.writer) & (db.comics_writers.comics == response.comics_id)).select()
    response.artists = db((db.person.id == db.comics_artists.artist) & (db.comics_artists.comics == response.comics_id)).select()
    response.user_boxes = db(db.box.owner == auth.user_id).select()
    response.unfiled = db((db.box.name == 'Unfiled') & (db.box.owner == auth.user_id)).select().first()

    response.owner = db(db.auth_user.id == response.box.owner).select().first()
    response.collection = db(db.comics_boxes.box == response.box.id).select(join=db.comics.on(db.comics.id==db.comics_boxes.comics))

    return dict()
