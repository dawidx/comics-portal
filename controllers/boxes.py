#Exam No: Y0077439
@auth.requires_login()
def create():
    form = SQLFORM(db.box, fields=['name', 'public'])
    form.vars.owner = auth.user_id
    import datetime
    form.vars.created = datetime.datetime.now()
    if form.process(onvalidation=validate_name, keepvalues=True).accepted:
        redirect(URL('boxes', 'view', args=[form.vars.id], vars=dict(message='newbox')))
    return dict(form=form)

@auth.requires_login()
def edit():
    box = db(db.box.id == request.args[0]).select().first()
    form = SQLFORM(db.box, box, fields=['name', 'public'])
    form.vars.owner = auth.user_id
    if form.process(onvalidation=validate_name, keepvalues=True).accepted:
        redirect(URL('boxes', 'view', args=[form.vars.id], vars=dict(message='editedbox')))
    return dict(form=form)

@auth.requires_login()
def all():
    response.comics = db(db.comics.owner == auth.user_id).select(join=[db.comics_boxes.on(db.comics_boxes.comics == db.comics.id)],groupby=db.comics_boxes.comics)
    get_user_boxes(auth.user_id)
    response.owner = db(db.auth_user.id == auth.user_id).select().first()
    response.unfiled = db((db.box.name == 'Unfiled') & (db.box.owner == auth.user_id)).select().first()
    response.current_user_boxes = db(db.box.owner == auth.user_id).select()
    return dict()

def validate_name(form):
    response.submitted = True

def get_user_boxes(owner):
    if owner == auth.user_id:
        response.user_boxes = db(db.box.owner == owner).select(orderby=db.box.name)
    else:
        response.user_boxes = db((db.box.owner == owner) & (db.box.public == "T")).select(orderby=db.box.name)


def visibility():
    box = db(db.box.id == request.args[0]).select().first()
    if box.public:
        db(db.box.id == request.args[0]).update(public=False)
        current='Private'
    else:
        db(db.box.id == request.args[0]).update(public=True)
        current='Public'

    redirect(URL('boxes', 'view', args=[request.args[0]],vars=dict(message='visibility', m_status=current)))


def view():
    response.box = db(db.box.id == request.args[0]).select().first()
    response.owner = db(db.auth_user.id == response.box.owner).select().first()
    if auth.user_id:
        response.unfiled = db((db.box.name == 'Unfiled') & (db.box.owner == auth.user_id)).select().first()
        response.current_user_boxes = db(db.box.owner == auth.user_id).select()
    response.comics = db(db.comics_boxes.box == response.box.id).select(join=db.comics_boxes.on(db.comics.id == db.comics_boxes.comics))
    get_user_boxes(response.box.owner)
    return dict()

@auth.requires_login()
def delete():
    #Get all the comics which belong to that box
    comics = db(db.comics_boxes.box == request.args[0]).select()
    box = db(db.box.id == request.args[0]).select().first()
    #delete records
    db(db.box.id == request.args[0]).delete()
    #Not the most optimal way
    #Getting the unfiled box for the user
    unfiled = db((db.box.name == 'Unfiled') & (db.box.owner == auth.user_id)).select().first()
    #This can potentially be pretty long
    for comic in comics:
        if not db(db.comics_boxes.comics == comic.comics).count():
            db.comics_boxes.insert(comics=comic.comics,box=unfiled.id)

    redirect(URL('view', args=[unfiled.id], vars=dict(message='deleted_box',m_name=box.name,prev_box=box.name)))

@auth.requires_login()
def my():
    redirect(URL('boxes', 'index', args=[auth.user_id]))

def index():
    if int(request.args[0]) == auth.user_id:
        first = db(db.box.owner == request.args[0]).select().first()
    else:
        first = db((db.box.owner == request.args[0]) & (db.box.public == "T")).select().first()
    redirect(URL('boxes', 'view', args=[first.id]))
