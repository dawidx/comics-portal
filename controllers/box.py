import datetime


@auth.requires_login()
def create():
    form = SQLFORM(db.box, fields=['name', 'public'])
    form.vars.user = auth.user_id
    form.vars.created = datetime.datetime.now()

    form.process(onvalidation=validate_name, keepvalues=True)

    response.user_boxes = db(db.box.user == auth.user_id).select(orderby=db.box.name)
    return dict(form=form)

def validate_name(form):
    response.submitted = True

def visibility():
    box = db(db.box.id == request.vars.id).select().first()
    if box.public:
        db(db.box.id == request.vars.id).update(public=False)
    else:
        db(db.box.id == request.vars.id).update(public=True)

    redirect(URL('box', 'view', vars=dict(id=request.vars.id,message=1)))


@auth.requires_login()
def delete():
    #Not the most optimal way
    #Getting the unfiled box for the user
    unfiled = db((db.box.name == 'Unfiled') & (db.box.user == auth.user_id)).select().first()
    #DELETE instances of the comics in the box to be deleted
    comics = db(db.comicsboxes.box == request.vars.id).select()
    #delete records
    db(db.comicsboxes.box == request.vars.id).delete()
    #insert the entries as
    for comic in comics:
        db.insert(comic=comic.id,box=unfiled.id)

    box = db(db.box.id == request.vars.id).select().first()
    db(db.box.id == request.vars.id).delete()
    redirect(URL('view', vars=dict(id=unfiled.id,message=2,prev_box=box.name)))

@auth.requires_login()
def edit():
    box = db(db.box.id == request.get_vars.id).select().first() or redirect(URL('/error'));
    form = SQLFORM(db.box, box,fields=['name', 'public'])
    response.box = box

    form.process(onvalidation=validate_name, keepvalues=True)

    response.user_boxes = db(db.box.user == auth.user_id).select()
    return dict(form=form)

def view():
    response.box = db(db.box.id == request.get_vars.id).select().first()
    response.comics = db(db.comicboxes.box == request.get_vars.id).select(join=db.comic.on(db.comicboxes.comic == db.comic.id))
    if response.box.user == auth.user_id:
        response.user_boxes = db(db.box.user == response.box.user).select(orderby=db.box.name)
    else:
        response.user_boxes = db((db.box.user == response.box.user) & (db.box.public == "T")).select(orderby=db.box.name)

    response.message = int(request.vars.message) if request.vars.message else 0
    if response.message == 4:
        response.prev_box = db(db.box.id == request.vars.box).select().first()
        response.prev_comics = db(db.comic.id == request.vars.comics).select().first()
    else:
        response.prev_box = request.vars.prev_box

    response.unfiled = db((db.box.name=="Unfiled") & (db.box.user == auth.user_id)).select().first()
    return dict()

@auth.requires_login()
def all():
    #HERE
    response.comics = db(db.comic.owner == auth.user_id).select()
    return dict()

@auth.requires_login()
def collection():
    redirect(URL('index'))


def index():
    #The users who don't have any public boxes are cannot be accessed
    if request.vars.id:
        box = db((db.box.user == request.vars.id) & (db.box.public =="T")).select().first()
    else:
        box = db(db.box.user == auth.user_id).select().first()
    redirect(URL('view', vars=dict(id=box.id)))
