#Exam No: Y0077439
import datetime

def signin():
    response.title = "Sign In to existing account"
    #by default will redirect to boxes
    if not request.vars._next:
        auth.settings.login_next = URL('boxes', 'my')
    else:
        response.message = 1
    auth.settings.on_failed_authentication = lambda url: redirect(url)
    return dict(form=auth.login())

def signup():
    response.title = "Join Longboxes Community"
    return dict(form=auth.register(onaccept=lambda form: (updateInfo(form))))

def updateInfo(form):
    user = db(db.auth_user.username == form.vars.username).select().first()
    db.box.insert(name='Unfiled', owner = user.id, created =datetime.datetime.now(), public = False)

def profile():
    response.title = "Your Profile"
    return dict(form=auth.profile())

def signout():
    return dict(form=auth.logout())
