# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################
#Exam No: Y0077439
def index():
    response.newest = db(db.box.public == "T").select(orderby=~db.box.created,limitby=(0,5))
    response.largest = db.executesql("SELECT box.name, box.id, COUNT(comics_boxes.comics) as 'comics' FROM box, comics_boxes WHERE box.id = comics_boxes.box AND box.public = 'T' GROUP BY box.id ORDER BY comics DESC",as_dict=True)
    return dict()

def search():
    q = request.get_vars.q if request.get_vars.q else ''
    response.results = []
    query = """SELECT comics.id, comics.title, comics.cover, comics.issue_no, box.id as box, auth_user.name, auth_user.username, auth_user.id as user_id
                FROM comics, box, auth_user, comics_boxes, publisher, comics_writers, comics_artists
                WHERE box.owner = auth_user.id
            		AND comics_boxes.comics = comics.id
                    AND comics_boxes.box = box.id
            		AND comics_boxes.box IN (SELECT box.id FROM box WHERE public = 'T' OR owner = " + (str(auth.user_id) if auth.user_id else 'null') + ")
            		AND (comics.title LIKE '%"""+q+"""%'
                        OR (publisher.name LIKE '%"""+q+"""%' AND comics.publisher = publisher.id)
                        OR (comics_writers.writer IN (SELECT person.id FROM person WHERE name LIKE '%"""+q+"""%') AND comics_writers.comics = comics.id)
                        OR (comics_artists.artist IN (SELECT person.id FROM person WHERE name LIKE '%"""+q+"""%') AND comics_artists.comics = comics.id))
                        GROUP BY comics.id""";
    response.results = db.executesql(query,as_dict = True)

    if request.vars.message:
        response.message = request.vars.message
        response.box = request.vars.box
        response.comics = request.vars.comics

    if auth.user_id:
        response.unfiled = db((db.box.owner== auth.user_id) & (db.box.name == 'Unfiled')).select().first()
        response.current_user_boxes = db((db.box.owner == auth.user_id)).select()

    return dict()

@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)

def prepare():
    #THIS IS FOR TESTING PURPOSES
    #DELETES THE ENTIRE DATABASE
    import shutil
    import os
    shutil.rmtree('/home/dawid/Desktop/iapt/web2py/applications/Longboxes/databases')
    os.makedirs('/home/dawid/Desktop/iapt/web2py/applications/Longboxes/databases')
    redirect(URL('setup'))

def setup():
    #ENTERS DEFAULT VALUES
    db.executesql("INSERT INTO person(name) VALUES ('Alan Moore'), ('Al Feldstein'),('Jim Shooter'),('Gardner Fax'),('John Byrne'),('Joe Gill'),('Edmond Hamilton'),('Otto Binder'),('Roy Thomas'),('Stan Lee')")
    db.executesql("INSERT INTO person(name) VALUES ('John Romita Jr.'), ('Brian Bolland'),('Will Eisner'),('Jim Steranko'),('Osamu Tezuka'),('Steve Ditko'),('Frank Miller'),('Dave Gibbons'),('Steve Dillon'), ('Jack Kirby')")
    db.executesql("INSERT INTO publisher(name) VALUES ('DC'),('Marvel'),('TJ Comics'),('IDW')")
    return dict()
