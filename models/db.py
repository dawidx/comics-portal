#Exam No: Y0077439
from gluon.contrib.appconfig import AppConfig
## once in production, remove reload=True to gain full speed
myconf = AppConfig(reload=True)

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## choose a style for forms
response.formstyle = myconf.take('forms.formstyle')  # or 'bootstrap3_stacked' or 'bootstrap2' or other
response.form_label_separator = myconf.take('forms.separator')

from gluon.tools import Auth, Service, PluginManager


db = DAL('sqlite://storage.db')
auth = Auth(db)
service = Service()
plugins = PluginManager()

db.define_table(
    auth.settings.table_user_name,
    Field('name', length=128, default=''),
    Field('username', length=128, default='', unique=True), # required
    Field('password', 'password', length=512,            # required
          readable=False, label='Password'),
    Field('registration_key', length=512,                # required
          writable=False, readable=False, default=''),
    Field('reset_password_key', length=512,              # required
          writable=False, readable=False, default=''),
    Field('registration_id', length=512,                 # required
          writable=False, readable=False, default=''))

#VALIDATORS
custom_auth_table = db[auth.settings.table_user_name] # get the custom_auth_table
custom_auth_table.username.requires = [IS_NOT_EMPTY(error_message=T('Username cannot be empty.')),IS_NOT_IN_DB(db, custom_auth_table.username, error_message=T('The username already exists. Try a different one.'))]
custom_auth_table.password.requires = [CRYPT(min_length=6, error_message=T('The password is too short'))]

#AUTH settings
auth.settings.table_user = custom_auth_table # tell auth to use custom_auth_table
auth.settings.controller = 'account'
auth.settings.login_url = URL('account', 'signin')

auth.define_tables(username=True, signature=False)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' if request.is_local else myconf.take('smtp.server')
mail.settings.sender = myconf.take('smtp.sender')
mail.settings.login = myconf.take('smtp.login')

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

########################################################################################

#person
db.define_table('person',
    Field('name', type='string', unique=True, notnull=True)
)
#Publisher
db.define_table('publisher',
    Field('name', type='string', unique=True, notnull=True)
)
#Box
db.define_table('box',
    Field('name', type='string', length=25, notnull=True),
    Field('owner', db.auth_user),
    Field('public', type='boolean', default=False, notnull=True),
    Field('created', type='datetime', notnull=True)
)
#Comic
db.define_table('comics',
    Field('cover', type='upload', default='uploads/no_cover.png', uploadfield=True),
    Field('title', type='string'),
    Field('issue_no', type="integer"),
    Field('description', type='text'),
    Field('publisher', db.publisher),
    Field('owner', db.auth_user)
)

#MANY TO MANY RELATIONS
db.define_table('comics_writers',
    Field('comics', db.comics),
    Field('writer', db.person)
)
db.define_table('comics_artists',
    Field('comics', db.comics),
    Field('artist', db.person)
)
db.define_table('comics_boxes',
    Field('comics', db.comics),
    Field('box', db.box)
)
